# GPT4chat

_Note:_ This is a hiring assignment.

## Architecture

I attempted to segregate the application into the following subcomponents:

- **Data Access Layer:**
  - Used `SQLAlchemy` as the DB engine with the repository pattern.
- **Service Layer:**
  - Employed `g4f` as the AI engine.
  - Utilized `repository.py` as the persistence layer.
- **Edge Layer:**
  - Implemented `FastAPI` as the HTTP front end.
  - Leveraged `OpenAPI` specs as documents that come with FastAPI as a bonus.
  - Used `BackgroundTasks` to respond to the user immediately while waiting for
    AI to generate a response. If necessary, this part can be exported to an
    external message queue (e.g., RabbitMQ) for horizontal scaling.

## Disclaimer

- `FastAPI` is not one of my strong skills. I encountered difficulty in making
  it work as expected, and I spent approximately 10 hours learning it (along
  with `pydantic`) from scratch before delving into the assignment.
- I could have created better tests in different layers with various purposes if
  I had enough time and energy:
  - E2E tests are already in place.
  - Unit tests can properly assess repository and AI engine behavior.
- The endpoints are currently tied to the repository for the sake of simplicity.
  However, if needed, it would require yet another layer of abstraction (e.g.,
  services) to enhance extensibility.
