import g4f

from .schemas import Message, Setting


def generate_response(setting: Setting, messages: list[Message]) -> str:
    model, provider = g4f.get_model_and_provider(
        setting.ai_model, provider=None, stream=False
    )
    response = g4f.ChatCompletion.create(
        model=model,
        provider=provider,
        messages=[
            {"role": str(setting.role), "content": setting.prompt},
            *[{"role": str(msg.role), "content": msg.content} for msg in messages],
        ],
    )
    return str(response)
