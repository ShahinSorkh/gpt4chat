from datetime import datetime
from enum import Enum
from typing import Annotated

import g4f
from pydantic import AfterValidator, AliasChoices, BaseModel, ConfigDict, Field
from pydantic.types import UUID4


class Role(Enum):
    SYSTEM = "system"
    HUMAN = "human"
    AI = "ai"


class MessageBase(BaseModel):
    content: str


class MessageCreate(MessageBase):
    pass


class Message(MessageBase):
    model_config = ConfigDict(from_attributes=True)

    id: UUID4
    role: Role
    created_at: datetime


def validate_ai_model(v: str) -> str:
    assert v in g4f.ModelUtils.convert
    return v


AiModel = Annotated[str, AfterValidator(validate_ai_model)]


class Setting(BaseModel):
    ai_model: AiModel = Field(
        alias="model_name",
        validation_alias=AliasChoices("model_name", "ai_model"),
        examples=["gpt-3.5-turbo"],
    )
    role: Role
    prompt: str


class ChatBase(Setting):
    pass


class ChatCreate(ChatBase):
    pass


class Chat(ChatBase):
    model_config = ConfigDict(from_attributes=True)

    id: UUID4
    created_at: datetime
    updated_at: datetime
    messages: list[Message] = []


class ChatResponse(BaseModel):
    id: UUID4
    setting: Setting
    created_at: datetime
    updated_at: datetime
    messages: list[Message] = []

    @classmethod
    def model_from_chat(cls, chat: Chat):
        return cls(**chat.model_dump(), setting=Setting(**chat.model_dump()))
