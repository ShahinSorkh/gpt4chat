from uuid import uuid4

from sqlalchemy import Column, DateTime, Enum, ForeignKey, String, Text, func
from sqlalchemy.orm import relationship

from .db import Base
from .schemas import Role


class Mixin:
    id = Column(
        String(34),
        primary_key=True,
        index=True,
        default=uuid4,
    )
    created_at = Column(DateTime, server_default=func.now())


class Chat(Base, Mixin):
    __tablename__ = "chats"

    updated_at = Column(DateTime, server_default=func.now(), onupdate=func.now())
    ai_model = Column(String)
    role = Column(Enum(Role))
    prompt = Column(Text)

    messages = relationship("Message")


class Message(Base, Mixin):
    __tablename__ = "messages"

    chat_id = Column(
        String(34),
        ForeignKey(Chat.id),
        primary_key=True,
        index=True,
        default=uuid4,
    )
    role = Column(Enum(Role))
    content = Column(Text)
