import pytest
from anyio import sleep
from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)


def test_create_chat():
    resp = client.post(
        "/chats",
        json={
            "model_name": "gpt-3.5-turbo",
            "role": "system",
            "prompt": "you are my personal ai assistant",
        },
    )
    assert resp.status_code == 201
    resp_body = resp.json()
    assert resp_body["id"] is not None

    resp = client.get(f'/chats/{resp_body["id"]}')
    assert resp.status_code == 200
    resp_body = resp.json()
    assert resp_body["setting"]["role"] == "system"

    resp = client.get("/chats")
    assert resp.status_code == 200
    assert len(resp.json()) >= 1


@pytest.mark.anyio
async def test_create_message():
    resp = client.post(
        "/chats",
        json={
            "model_name": "gpt-3.5-turbo",
            "role": "system",
            "prompt": "you are my personal ai assistant",
        },
    )
    assert resp.status_code == 201
    chat_id = resp.json()["id"]

    resp = client.post(f"/chats/{chat_id}/messages", json={"content": "Hello ai!"})
    assert resp.status_code == 201
    resp_body = resp.json()
    assert resp_body["id"] is not None
    assert resp_body["role"] == "human"

    await sleep(2)

    resp = client.get(f"/chats/{chat_id}/messages")
    assert resp.status_code == 200
    assert len(resp.json()) == 2
