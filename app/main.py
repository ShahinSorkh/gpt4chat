from fastapi import BackgroundTasks, FastAPI, HTTPException
from pydantic.types import UUID4

from . import ai, models, repository
from .db import dbdepends, engine
from .schemas import ChatCreate, ChatResponse, Message, MessageCreate

models.Base.metadata.create_all(bind=engine)
app = FastAPI()


@app.get("/chats")
def index_chats(db: dbdepends) -> list[ChatResponse]:
    return [ChatResponse.model_from_chat(chat) for chat in repository.get_all_chats(db)]


@app.post("/chats", status_code=201)
def create_new_chat(create_chat: ChatCreate, db: dbdepends) -> ChatResponse:
    chat = repository.create_chat(db, create_chat)
    return ChatResponse.model_from_chat(chat)


@app.get("/chats/{chat_id}")
def get_chat(chat_id: UUID4, db: dbdepends) -> ChatResponse:
    chat = repository.find_chat(db, chat_id)
    if chat is None:
        raise HTTPException(status_code=404)
    return ChatResponse.model_from_chat(chat)


def append_ai_response(db: dbdepends, chat: ChatResponse):
    ai_msg = ai.generate_response(chat.setting, chat.messages)
    repository.create_ai_message(db, chat.id, ai_msg)


@app.post("/chats/{chat_id}/messages", status_code=201)
def create_new_message(
    chat_id: UUID4,
    body: MessageCreate,
    bg_tasks: BackgroundTasks,
    db: dbdepends,
) -> Message:
    chat = get_chat(chat_id, db)
    msg = repository.create_human_message(db, chat_id, body.content)
    assert msg is not None  # `get_chat` already raises 404 if chat does not exist

    bg_tasks.add_task(append_ai_response, db, chat)

    return msg


@app.get("/chats/{chat_id}/messages")
def get_chat_messages(chat_id: UUID4, db: dbdepends) -> list[Message]:
    return repository.get_chat_messages(db, chat_id)


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
