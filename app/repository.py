from typing import Optional
from uuid import uuid4

from pydantic import UUID4
from sqlalchemy.orm import Session

from . import models, schemas


def get_all_chats(db: Session) -> list[schemas.Chat]:
    return [
        schemas.Chat.model_validate(chat)
        for chat in db.query(models.Chat).order_by(models.Chat.created_at.desc()).all()
    ]


def find_chat(db: Session, chat_id: UUID4) -> Optional[schemas.Chat]:
    chat = db.query(models.Chat).filter(models.Chat.id == str(chat_id)).first()
    return schemas.Chat.model_validate(chat)


def create_chat(db: Session, create_chat: schemas.ChatCreate) -> schemas.Chat:
    chat = models.Chat(
        id=str(uuid4()),
        **create_chat.model_dump(),
    )
    db.add(chat)
    db.commit()
    db.refresh(chat)
    return schemas.Chat.model_validate(chat)


def _create_message(
    db: Session, chat_id: UUID4, role: schemas.Role, content: str
) -> Optional[schemas.Message]:
    chat = db.query(models.Chat).filter(models.Chat.id == str(chat_id)).first()
    if chat is None:
        return None
    msg = models.Message(
        id=str(uuid4()),
        chat_id=str(chat.id),
        role=role,
        content=content,
    )
    db.add_all([chat, msg])
    db.commit()
    db.refresh(msg)
    return schemas.Message.model_validate(msg)


def create_ai_message(
    db: Session, chat_id: UUID4, content: str
) -> Optional[schemas.Message]:
    return _create_message(db, chat_id, schemas.Role.AI, content)


def create_human_message(
    db: Session, chat_id: UUID4, content: str
) -> Optional[schemas.Message]:
    return _create_message(db, chat_id, schemas.Role.HUMAN, content)


def get_chat_messages(db: Session, chat_id: UUID4) -> list[schemas.Message]:
    return [
        schemas.Message.model_validate(msg)
        for msg in db.query(models.Message)
        .filter(models.Message.chat_id == str(chat_id))
        .order_by(models.Message.created_at.desc())
        .all()
    ]
